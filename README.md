# XNAT ROI Plugin 1.0.0 #

This plugin adds ROI collections as a data type to XNAT 1.7. Currently supported types: NIH/NCI's [AIM](https://wiki.nci.nih.gov/display/AIM/Annotation+and+Image+Markup+-+AIM) and DICOM RTSTRUCT. The plugin is licensed under the BSD license, see LICENSE file.

**Please note: this plugin changes the server's schema.**

[TOC]

## Definitions ##

* **ROI collection:** A single entity, normally a file, that contains one or more ROIs.
* **ROI:** A contiguous 3D volume defined by one or more closed planar contour.
* **Collection type:** the data type (or format) of the ROI collection. May be **AIM** or **RTSTRUCT**.


## Building the Plugin ##

1. Clone the repository
2. Build:
	```./gradlew clean fatjar```
	or 
	```gradlew clean fatjar```
	on Windows
3. Grab the **build/libs/xnat-roi-plugin-fatjar-x.y.z-SNAPSHOT.jar** file.


## Deployment ##

1. Copy **xnat-roi-plugin-fatjar-1.0.0-SNAPSHOT.jar** into XNAT's plugins directory. This is **/data/xnat/home/plugins** by default.
2. Restart Tomcat
	```sudo service tomcat7 restart```


## Diagnostics ##

The plugin logs to **xnat-roi.log** in XNAT's log directory. This is **/data/xnat/home/logs** by default. Effort has been made to make the logged information specific and helpful. The logging configuration may be altered by editing **xnat-roi-log4j.properties** inside the plugin jar file and restarting Tomcat. This is done at the user's risk.


## XAPI ##

All interaction with the plugin is via XNAT's XAPI REST functions e.g.

```GET /xapi/roi/projects/{projectId}/collections/{idOrLabel}```


### Path Parameters ###

* **projectId** is checked for appropriate permissions for the operation. 403 (Forbidden) is returned on failure.

* **sessionId** is checked for appropriate permissions for the operation. 403 (Forbidden) is returned on failure. 422 (UnprocessableEntity) if no such session exists.

* **idOrLabel** is checked for appropriate permissions for the operation. 403 (Forbidden) is returned on failure. 422 (UnprocessableEntity) if no such ID or label exists.


### Return Codes ###

HTTP return codes have their normal meaning unless expanded upon here.

* **422 (UnprocessableEntity)** is returned when a request is well formed but cannot be fulfilled for some reason. This includes but is not limited to: an item with the requested ID doesn't exist or an unsupported collection type was specified.

* **500 (InternalServerError)** is returned when XNAT's backend throws an exception or a RuntimeException is caught by the plugin.